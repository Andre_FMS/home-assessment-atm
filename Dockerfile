FROM openjdk:11-jre-slim-buster
MAINTAINER andresilva1997
COPY build/libs/*.jar atm-project-1.0.0.jar
ENTRYPOINT ["java","-jar","/atm-project-1.0.0.jar"]