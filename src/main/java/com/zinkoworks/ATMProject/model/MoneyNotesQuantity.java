package com.zinkoworks.ATMProject.model;

import com.zinkoworks.ATMProject.model.utils.MoneyNotes;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class MoneyNotesQuantity {

    @Id
    private MoneyNotes id;

    private Integer quantity;

}
