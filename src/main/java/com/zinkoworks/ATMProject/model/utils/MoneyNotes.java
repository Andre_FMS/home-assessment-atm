package com.zinkoworks.ATMProject.model.utils;

public enum MoneyNotes {
    FIVE(5),
    TEN(10),
    TWENTY(20),
    FIFTY(50),
    HUNDRED(100),
    TWO_HUNDRED(200),
    FIVE_HUNDRED(500);

    private final int value;

    MoneyNotes(final int newValue) {
        value = newValue;
    }

    public int getValue() {
        return value;
    }
}
