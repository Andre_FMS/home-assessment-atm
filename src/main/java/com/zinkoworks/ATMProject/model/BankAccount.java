package com.zinkoworks.ATMProject.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
public class BankAccount {

    @Id
    private String number;

    private String pin;

    private Double balance;

    private Double overDraft;

}
