package com.zinkoworks.ATMProject.repository;


import com.zinkoworks.ATMProject.model.MoneyNotesQuantity;
import com.zinkoworks.ATMProject.model.utils.MoneyNotes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
public interface NotesQuantityRepository extends JpaRepository<MoneyNotesQuantity, MoneyNotes> {

    default Map<MoneyNotes, Integer> findAllMap() {
        return findAll().stream().collect(Collectors.toMap(MoneyNotesQuantity::getId, MoneyNotesQuantity::getQuantity));
    }

    default List<MoneyNotesQuantity> saveAllMap(Map<MoneyNotes, Integer> map) {
        List<MoneyNotesQuantity> list = map.entrySet().stream().map(x -> new MoneyNotesQuantity(x.getKey(), x.getValue())).collect(Collectors.toList());
        return saveAll(list);
    }

}
