package com.zinkoworks.ATMProject.dto;

import com.zinkoworks.ATMProject.exceptions.InputErrorException;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.h2.util.StringUtils;

@Data
@AllArgsConstructor
public class BankAccountLogin {
    private String accountNumber;
    private String pin;


    public void validate() throws InputErrorException {

        if (!StringUtils.isNumber(accountNumber) || Integer.parseInt(accountNumber) < 0) {
            throw new InputErrorException("AccountNumber invalid");
        }
        if (!StringUtils.isNumber(pin) || pin.length() != 4 || Integer.parseInt(pin) < 0 || Integer.parseInt(pin) > 9999) {
            throw new InputErrorException("Pin must be 4 digits");
        }


    }
}
