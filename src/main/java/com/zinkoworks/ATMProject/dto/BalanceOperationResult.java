package com.zinkoworks.ATMProject.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class BalanceOperationResult {

    private Double currentBalance;
    private Integer maxWithdrawAvailable;

}


