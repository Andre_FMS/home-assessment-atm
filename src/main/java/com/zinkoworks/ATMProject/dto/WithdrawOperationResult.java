package com.zinkoworks.ATMProject.dto;


import com.zinkoworks.ATMProject.model.utils.MoneyNotes;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Map;

@Getter
@AllArgsConstructor
public class WithdrawOperationResult {

    private Double currentBalance;
    private Map<MoneyNotes, Integer> moneyNotesQuantityMap;

}
