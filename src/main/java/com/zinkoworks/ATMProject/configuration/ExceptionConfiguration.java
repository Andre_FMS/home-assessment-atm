package com.zinkoworks.ATMProject.configuration;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.zinkoworks.ATMProject.exceptions.InputErrorException;
import com.zinkoworks.ATMProject.exceptions.NotEnoughBalanceException;
import com.zinkoworks.ATMProject.exceptions.NotEnoughChangeException;
import com.zinkoworks.ATMProject.exceptions.WrongPinException;
import lombok.Data;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.persistence.EntityNotFoundException;
import java.io.Serializable;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;


@ControllerAdvice
public class ExceptionConfiguration extends ResponseEntityExceptionHandler {

    private static final Class[] NOT_FOUND_EXCEPTIONS = {
            EntityNotFoundException.class
    };

    private static final Class[] CONFLICT_EXCEPTIONS = {
            NotEnoughBalanceException.class,
            NotEnoughChangeException.class
    };

    private static final Class[] BAD_REQUEST_EXCEPTIONS = {
            InputErrorException.class,
            WrongPinException.class,
    };


    @ExceptionHandler({Exception.class})
    public ResponseEntity<Object> handleAll(Exception exception, WebRequest request) {
        HttpHeaders httpHeaders = new HttpHeaders();

        List<Class> notFound = Arrays.asList(NOT_FOUND_EXCEPTIONS);
        List<Class> conflict = Arrays.asList(CONFLICT_EXCEPTIONS);
        List<Class> badRequest = Arrays.asList(BAD_REQUEST_EXCEPTIONS);

        HttpStatus status;
        if (notFound.contains(exception.getClass())) {
            status = HttpStatus.NOT_FOUND;
        } else if (conflict.contains(exception.getClass())) {
            status = HttpStatus.CONFLICT;
        } else if (badRequest.contains(exception.getClass())) {
            status = HttpStatus.BAD_REQUEST;
        } else {
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        }

        ErrorResponseBody error = new ErrorResponseBody();
        error.setException(exception.getClass().getSimpleName());
        error.setMessage(exception.getMessage());
        error.setErrorCode(status.value());
        error.setPath(getContextPath(request));

        return new ResponseEntity<>(error, new HttpHeaders(), error.getErrorCode());


    }

    private String getContextPath(WebRequest request) {
        return Optional.ofNullable(request)
                .filter(ServletWebRequest.class::isInstance)
                .map(ServletWebRequest.class::cast)
                .map(swr -> swr.getRequest().getRequestURI())
                .orElse(null);
    }


    @JsonIgnoreProperties(ignoreUnknown = true)
    @Data
    private static class ErrorResponseBody implements Serializable {

        private ZonedDateTime timestamp = Instant.now().atZone(ZoneId.of("UTC"));
        private String exception;
        private Integer errorCode;
        private String path;
        private String message;

    }

}

