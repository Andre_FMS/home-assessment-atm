package com.zinkoworks.ATMProject.controller.ui;

import com.zinkoworks.ATMProject.dto.BalanceOperationResult;
import com.zinkoworks.ATMProject.dto.BankAccountLogin;
import com.zinkoworks.ATMProject.dto.WithdrawOperationResult;
import com.zinkoworks.ATMProject.exceptions.InputErrorException;
import com.zinkoworks.ATMProject.exceptions.NotEnoughBalanceException;
import com.zinkoworks.ATMProject.exceptions.NotEnoughChangeException;
import com.zinkoworks.ATMProject.exceptions.WrongPinException;
import com.zinkoworks.ATMProject.model.utils.MoneyNotes;
import com.zinkoworks.ATMProject.service.AtmMachineService;
import com.zinkoworks.ATMProject.service.BankAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.persistence.EntityNotFoundException;
import java.util.*;

@Controller
public class ATMUIController {


    @Autowired
    AtmMachineService atmMachineService;

    @Autowired
    BankAccountService bankAccountService;

    @GetMapping("/home")
    public String welcome(Model model) {
        return "menu";
    }


    @GetMapping("/login")
    public String login(@RequestParam String operationType, @Nullable @RequestParam Integer ammount, Model model) {
        model.addAttribute("operationType", operationType);
        model.addAttribute("ammount", ammount);
        return "login";
    }


    @PostMapping("/balance")
    public String getBalance(@ModelAttribute BankAccountLogin bankAccountLogin, Model model, RedirectAttributes redirectAttributes) {
        try {
            BalanceOperationResult balance = atmMachineService.getBalance(bankAccountLogin);
            model.addAttribute("balance", balance.getCurrentBalance());
            model.addAttribute("maxWithdrawAmmount", balance.getMaxWithdrawAvailable());
            return "balance";
        } catch (WrongPinException | EntityNotFoundException | InputErrorException e) {
            redirectAttributes.addFlashAttribute("errorMessage", e.getMessage());
            return "redirect:/login?operationType=balance";
        }
    }


    @PostMapping("/withdraw")
    public String withdrawMoney(@RequestParam Integer ammount,
                                @ModelAttribute BankAccountLogin bankAccountLogin, Model model, RedirectAttributes redirectAttributes) {
        try {
            WithdrawOperationResult withdrawOperationResult = atmMachineService.withdrawMoney(bankAccountLogin, ammount);
            Map<MoneyNotes, Integer> money = withdrawOperationResult.getMoneyNotesQuantityMap();
            List<Map.Entry<MoneyNotes, Integer>> moneySorted = new ArrayList<>(money.entrySet());
            Collections.sort(moneySorted, Comparator.comparing(Map.Entry::getKey));
            model.addAttribute("money", moneySorted);
            model.addAttribute("balance", withdrawOperationResult.getCurrentBalance());
            return "withdraw";
        } catch (WrongPinException | EntityNotFoundException | InputErrorException exception) {
            redirectAttributes.addFlashAttribute("errorMessage", exception.getMessage());
            return "redirect:/login?operationType=withdraw&ammount=" + ammount;
        } catch (NotEnoughBalanceException | NotEnoughChangeException exception) {
            redirectAttributes.addFlashAttribute("errorMessage", exception.getMessage());
            return "redirect:/home";
        }
    }


}
