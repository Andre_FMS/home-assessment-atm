package com.zinkoworks.ATMProject.controller.api;

import com.zinkoworks.ATMProject.dto.BalanceOperationResult;
import com.zinkoworks.ATMProject.dto.BankAccountLogin;
import com.zinkoworks.ATMProject.dto.WithdrawOperationResult;
import com.zinkoworks.ATMProject.exceptions.InputErrorException;
import com.zinkoworks.ATMProject.exceptions.NotEnoughBalanceException;
import com.zinkoworks.ATMProject.exceptions.NotEnoughChangeException;
import com.zinkoworks.ATMProject.exceptions.WrongPinException;
import com.zinkoworks.ATMProject.service.AtmMachineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityNotFoundException;

@RestController
@RequestMapping("/api/account/")
public class UserController {

    @Autowired
    AtmMachineService atmMachineService;

    @GetMapping("balance")
    public BalanceOperationResult getBalance(@RequestParam String accountNumber, @RequestParam String pin) throws WrongPinException, InputErrorException {
        return atmMachineService.getBalance(new BankAccountLogin(accountNumber, pin));
    }

    @GetMapping("withdraw")
    public WithdrawOperationResult withdrawMoney(@RequestParam String accountNumber, @RequestParam String pin, @RequestParam Integer ammount) throws WrongPinException, EntityNotFoundException, NotEnoughChangeException, NotEnoughBalanceException, InputErrorException {
        return atmMachineService.withdrawMoney(new BankAccountLogin(accountNumber, pin), ammount);
    }


}
