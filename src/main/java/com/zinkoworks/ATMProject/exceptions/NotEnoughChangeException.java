package com.zinkoworks.ATMProject.exceptions;

public class NotEnoughChangeException extends Exception {
    public NotEnoughChangeException(String message) {
        super(message);
    }
}
