package com.zinkoworks.ATMProject;

import com.zinkoworks.ATMProject.model.BankAccount;
import com.zinkoworks.ATMProject.model.MoneyNotesQuantity;
import com.zinkoworks.ATMProject.model.utils.MoneyNotes;
import com.zinkoworks.ATMProject.service.AtmMachineService;
import com.zinkoworks.ATMProject.service.BankAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;

@Component
public class DataLoader implements ApplicationRunner {

    @Autowired
    AtmMachineService atmMachineService;

    @Autowired
    BankAccountService bankAccountService;

    @Transactional
    public void run(ApplicationArguments args) {

        atmMachineService.saveMoneyNotesQuantity(Arrays.asList(new MoneyNotesQuantity(MoneyNotes.FIVE, 20),
                new MoneyNotesQuantity(MoneyNotes.TEN, 30),
                new MoneyNotesQuantity(MoneyNotes.TWENTY, 30),
                new MoneyNotesQuantity(MoneyNotes.FIFTY, 10),
                new MoneyNotesQuantity(MoneyNotes.HUNDRED, 0),
                new MoneyNotesQuantity(MoneyNotes.TWO_HUNDRED, 0),
                new MoneyNotesQuantity(MoneyNotes.FIVE_HUNDRED, 0)));


        BankAccount bankAccount = new BankAccount();
        bankAccount.setBalance(800.0);
        bankAccount.setNumber("123456789");
        bankAccount.setPin("1234");
        bankAccount.setOverDraft(200.0);

        BankAccount bankAccount2 = new BankAccount();
        bankAccount2.setBalance(1230.0);
        bankAccount2.setNumber("987654321");
        bankAccount2.setPin("4321");
        bankAccount2.setOverDraft(150.0);
        bankAccountService.insertBankAccountBatch(Arrays.asList(bankAccount, bankAccount2));

    }

}
