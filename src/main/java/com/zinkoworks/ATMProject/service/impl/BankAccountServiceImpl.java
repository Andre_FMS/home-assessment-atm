package com.zinkoworks.ATMProject.service.impl;

import com.zinkoworks.ATMProject.dto.BankAccountLogin;
import com.zinkoworks.ATMProject.exceptions.InputErrorException;
import com.zinkoworks.ATMProject.exceptions.WrongPinException;
import com.zinkoworks.ATMProject.model.BankAccount;
import com.zinkoworks.ATMProject.repository.BankAccountRepository;
import com.zinkoworks.ATMProject.service.AtmMachineService;
import com.zinkoworks.ATMProject.service.BankAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
public class BankAccountServiceImpl implements BankAccountService {

    @Autowired
    BankAccountRepository bankAccountRepository;

    @Autowired
    AtmMachineService atmMachineService;

    @Override
    public List<BankAccount> insertBankAccountBatch(List<BankAccount> bankAccounts) {
        return bankAccountRepository.saveAll(bankAccounts);
    }

    @Override
    public BankAccount getBankAccount(BankAccountLogin bankAccountLogin) throws WrongPinException, EntityNotFoundException, InputErrorException {

        bankAccountLogin.validate();

        Optional<BankAccount> bankAccountOptional = bankAccountRepository.findById(bankAccountLogin.getAccountNumber());

        BankAccount bankAccount = bankAccountOptional
                .orElseThrow(() -> new EntityNotFoundException("Unable to find Bank Account with account number " + bankAccountLogin.getAccountNumber()));


        if (!bankAccount.getPin().equals(bankAccountLogin.getPin())) {
            throw new WrongPinException("You entered the wrong pin. Try again");
        }

        return bankAccount;


    }

    @Override
    public BankAccount updateBankAccountBalance(BankAccountLogin bankAccountLogin, Double newBalance) throws InputErrorException, WrongPinException {
        BankAccount bankAccount = this.getBankAccount(bankAccountLogin);

        if (newBalance < (-1 * bankAccount.getOverDraft())) {
            throw new InputErrorException("New Balance is too low");
        }

        bankAccount.setBalance(newBalance);
        return bankAccountRepository.save(bankAccount);
    }
}
