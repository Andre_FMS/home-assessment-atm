package com.zinkoworks.ATMProject.service.impl;

import com.zinkoworks.ATMProject.dto.BalanceOperationResult;
import com.zinkoworks.ATMProject.dto.BankAccountLogin;
import com.zinkoworks.ATMProject.dto.WithdrawOperationResult;
import com.zinkoworks.ATMProject.exceptions.InputErrorException;
import com.zinkoworks.ATMProject.exceptions.NotEnoughBalanceException;
import com.zinkoworks.ATMProject.exceptions.NotEnoughChangeException;
import com.zinkoworks.ATMProject.exceptions.WrongPinException;
import com.zinkoworks.ATMProject.model.BankAccount;
import com.zinkoworks.ATMProject.model.MoneyNotesQuantity;
import com.zinkoworks.ATMProject.model.utils.MoneyNotes;
import com.zinkoworks.ATMProject.repository.NotesQuantityRepository;
import com.zinkoworks.ATMProject.service.AtmMachineService;
import com.zinkoworks.ATMProject.service.BankAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.*;

@Service
public class AtmMachineServiceImpl implements AtmMachineService {

    @Autowired
    BankAccountService bankAccountService;

    @Autowired
    NotesQuantityRepository notesQuantityRepository;

    @Override
    @Transactional
    public List<MoneyNotesQuantity> saveMoneyNotesQuantity(List<MoneyNotesQuantity> notesQuantities) {
        return notesQuantityRepository.saveAll(notesQuantities);
    }

    @Override
    public BalanceOperationResult getBalance(BankAccountLogin bankAccountLogin) throws WrongPinException, EntityNotFoundException, InputErrorException {
        BankAccount bankAccount = bankAccountService.getBankAccount(bankAccountLogin);
        Integer maxAmmount = bankAccount.getBalance() < 0 ? 0 : (int) Math.min(getSumOfNotes(), (bankAccount.getBalance()+bankAccount.getOverDraft()));
        return new BalanceOperationResult(bankAccount.getBalance(), maxAmmount);
    }

    @Override
    @Transactional
    public WithdrawOperationResult withdrawMoney(BankAccountLogin bankAccountLogin, Integer ammount) throws WrongPinException, EntityNotFoundException, NotEnoughChangeException, NotEnoughBalanceException, InputErrorException {

        if (ammount < 0) {
            throw new InputErrorException("Ammont to withdraw must be positive");
        }
        BankAccount bankAccount = bankAccountService.getBankAccount(bankAccountLogin);


        if (bankAccount.getBalance() < 0) {
            throw new NotEnoughBalanceException("You cannot withdraw money because your account has a negative balance.");
        }
        if (bankAccount.getBalance() + bankAccount.getOverDraft() < ammount) {
            throw new NotEnoughBalanceException("Not enough balance in your bank account. Please choose a lower value to withdraw.");
        }

        Map<MoneyNotes, Integer> notesQuantityMap = notesQuantityRepository.findAllMap();


        Map<MoneyNotes, Integer> change = calculateChange(notesQuantityMap, ammount);


        for (Map.Entry<MoneyNotes, Integer> entry : change.entrySet()) {
            int newQtyOfNotes = notesQuantityMap.get(entry.getKey()) - change.get(entry.getKey());
            notesQuantityMap.put(entry.getKey(), newQtyOfNotes);
        }

        notesQuantityRepository.saveAllMap(notesQuantityMap);

        Double newAmmount = bankAccount.getBalance() - ammount;

        bankAccountService.updateBankAccountBalance(bankAccountLogin, newAmmount);

        return new WithdrawOperationResult(newAmmount, change);
    }

    @Override
    public Integer getSumOfNotes() {
        return notesQuantityRepository.findAll().stream().map(obj -> obj.getId().getValue() * obj.getQuantity()).reduce(Integer::sum).orElse(0);

    }

    private Map<MoneyNotes, Integer> calculateChange(Map<MoneyNotes, Integer> notesQuantityMap, Integer ammountToWithdraw) throws NotEnoughChangeException {
        List<Map.Entry<MoneyNotes, Integer>> list = new ArrayList<>(notesQuantityMap.entrySet());

        Collections.sort(list, Comparator.comparing(Map.Entry::getKey));
        Collections.reverse(list);
        //sorting the list to 500,200,100,50,20,10,5 bills

        int newAmount = ammountToWithdraw;
        Map<MoneyNotes, Integer> change =new EnumMap<>(MoneyNotes.class);
        for (Map.Entry<MoneyNotes, Integer> entry : list) {
            int moneyNoteValue = entry.getKey().getValue();
            int qtyOfMoneyNotesInATM = entry.getValue();
            int idealqtyOfMoneyNotes = newAmount / moneyNoteValue;
            //predicting that the machine n has the ideal amount of bills
            int qtyOfMoneyNotesToGive = Math.min(qtyOfMoneyNotesInATM, idealqtyOfMoneyNotes);
            newAmount = newAmount - (qtyOfMoneyNotesToGive * moneyNoteValue);
            change.put(entry.getKey(), qtyOfMoneyNotesToGive);
        }

        //if newAmmount is bigger than the machine cannot dispense the ammount requested
        if (newAmount > 0) {
            throw new NotEnoughChangeException("Not enough change in the ATM. Please choose a lower value to withdraw.");
        }

        return change;
    }


}
