package com.zinkoworks.ATMProject.service;

import com.zinkoworks.ATMProject.dto.BankAccountLogin;
import com.zinkoworks.ATMProject.exceptions.InputErrorException;
import com.zinkoworks.ATMProject.exceptions.WrongPinException;
import com.zinkoworks.ATMProject.model.BankAccount;

import javax.persistence.EntityNotFoundException;
import java.util.List;

public interface BankAccountService {

    List<BankAccount> insertBankAccountBatch(List<BankAccount> bankAccounts);


    BankAccount getBankAccount(BankAccountLogin bankAccountLogin) throws WrongPinException, EntityNotFoundException, InputErrorException;

    BankAccount updateBankAccountBalance(BankAccountLogin bankAccountLogin, Double newBalance) throws InputErrorException, WrongPinException;
}
