package com.zinkoworks.ATMProject.service;

import com.zinkoworks.ATMProject.dto.BalanceOperationResult;
import com.zinkoworks.ATMProject.dto.BankAccountLogin;
import com.zinkoworks.ATMProject.dto.WithdrawOperationResult;
import com.zinkoworks.ATMProject.exceptions.InputErrorException;
import com.zinkoworks.ATMProject.exceptions.NotEnoughBalanceException;
import com.zinkoworks.ATMProject.exceptions.NotEnoughChangeException;
import com.zinkoworks.ATMProject.exceptions.WrongPinException;
import com.zinkoworks.ATMProject.model.MoneyNotesQuantity;

import javax.persistence.EntityNotFoundException;
import java.util.List;

public interface AtmMachineService {

    List<MoneyNotesQuantity> saveMoneyNotesQuantity(List<MoneyNotesQuantity> notesQuantities);

    WithdrawOperationResult withdrawMoney(BankAccountLogin bankAccountLogin, Integer ammount) throws WrongPinException, EntityNotFoundException, NotEnoughChangeException, NotEnoughBalanceException, InputErrorException;

    BalanceOperationResult getBalance(BankAccountLogin bankAccountLogin) throws WrongPinException, EntityNotFoundException, InputErrorException;

    Integer getSumOfNotes();
}
