package com.zinkoworks.ATMProject.repository;

import com.zinkoworks.ATMProject.model.MoneyNotesQuantity;
import com.zinkoworks.ATMProject.model.utils.MoneyNotes;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.zinkoworks.ATMProject.utils.TestUtils.getMoneyNotesQuantityMock;
import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@DataJpaTest
class NotesQuantityRepositoryTest {

    @MockBean
    NotesQuantityRepository notesQuantityRepository;


    @BeforeEach
    void setUp() {
        Mockito.when(notesQuantityRepository.findAll()).thenReturn(getMoneyNotesQuantityMock());
        Mockito.when(notesQuantityRepository.findAllMap()).thenCallRealMethod();
        Mockito.when(notesQuantityRepository.saveAll(Mockito.any(List.class))).thenAnswer(i -> i.getArguments()[0]);
        Mockito.when(notesQuantityRepository.saveAllMap(Mockito.any())).thenCallRealMethod();
    }

    @Test
    void findAllMap() {
        List<MoneyNotesQuantity> list = notesQuantityRepository.findAll();
        Map<MoneyNotes, Integer> map = notesQuantityRepository.findAllMap();

        List<MoneyNotesQuantity> list2 = map.entrySet().stream().map(entry -> new MoneyNotesQuantity(entry.getKey(), entry.getValue())).collect(Collectors.toList());
        assertThat(list).hasSize(list2.size()).hasSameElementsAs(list2);
    }

    @Test
    void saveAllMap() {
        Map<MoneyNotes, Integer> map = notesQuantityRepository.findAllMap();

        List<MoneyNotesQuantity> list = notesQuantityRepository.findAll();
        List<MoneyNotesQuantity> list2 = notesQuantityRepository.saveAllMap(map);

        assertThat(list).hasSize(list2.size()).hasSameElementsAs(list2);

    }
}