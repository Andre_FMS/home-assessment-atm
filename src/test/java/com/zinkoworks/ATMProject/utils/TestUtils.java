package com.zinkoworks.ATMProject.utils;

import com.zinkoworks.ATMProject.dto.BankAccountLogin;
import com.zinkoworks.ATMProject.model.BankAccount;
import com.zinkoworks.ATMProject.model.MoneyNotesQuantity;
import com.zinkoworks.ATMProject.model.utils.MoneyNotes;
import org.junit.jupiter.api.function.Executable;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestUtils {

    public static void assertExceptionAndMessage(Class<? extends Exception> exceptionClass,
                                                 Executable executable,
                                                 String expectedMessage) {
        Exception exception = assertThrows(exceptionClass, executable);
        assertEquals(expectedMessage, exception.getMessage());
    }

    public static BankAccount getMockBankAccount() {
        BankAccount bankAccount = new BankAccount();
        bankAccount.setBalance(3000.0);
        bankAccount.setNumber("123456789");
        bankAccount.setPin("1234");
        bankAccount.setOverDraft(200.0);
        return bankAccount;
    }

    public static List<MoneyNotesQuantity> getMoneyNotesQuantityMock() {
        return Arrays.asList(new MoneyNotesQuantity(MoneyNotes.FIVE, 20),
                new MoneyNotesQuantity(MoneyNotes.TEN, 30),
                new MoneyNotesQuantity(MoneyNotes.TWENTY, 30),
                new MoneyNotesQuantity(MoneyNotes.FIFTY, 10),
                new MoneyNotesQuantity(MoneyNotes.HUNDRED, 0),
                new MoneyNotesQuantity(MoneyNotes.TWO_HUNDRED, 0),
                new MoneyNotesQuantity(MoneyNotes.FIVE_HUNDRED, 0));
    }

    public static BankAccountLogin generateBankAccountLogin(BankAccount bankAccount) {
        return new BankAccountLogin(bankAccount.getNumber(), bankAccount.getPin());
    }
}
