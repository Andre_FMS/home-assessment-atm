package com.zinkoworks.ATMProject.service;

import com.zinkoworks.ATMProject.dto.BankAccountLogin;
import com.zinkoworks.ATMProject.exceptions.InputErrorException;
import com.zinkoworks.ATMProject.exceptions.WrongPinException;
import com.zinkoworks.ATMProject.model.BankAccount;
import com.zinkoworks.ATMProject.repository.BankAccountRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityNotFoundException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static com.zinkoworks.ATMProject.utils.TestUtils.assertExceptionAndMessage;
import static com.zinkoworks.ATMProject.utils.TestUtils.getMockBankAccount;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
class BankAccountServiceTest {

    @Autowired
    BankAccountService bankAccountService;

    @MockBean
    AtmMachineService atmMachineService;

    @MockBean
    BankAccountRepository bankAccountRepository;


    @BeforeEach
    void setUp() {
        BankAccount bankAccount = getMockBankAccount();
        Mockito.when(bankAccountRepository.findById(ArgumentMatchers.matches(bankAccount.getNumber())))
                .thenReturn(Optional.of(bankAccount));
        Mockito.when(bankAccountRepository.save(Mockito.any(BankAccount.class))).thenAnswer(i -> i.getArguments()[0]);
        Mockito.when(bankAccountRepository.saveAll(Mockito.any(List.class))).thenAnswer(i -> i.getArguments()[0]);
        Mockito.when(atmMachineService.getSumOfNotes()).thenReturn(1000);
    }


    @Test
    @DisplayName("insertBankAccountBatch")
    void insertBankAccountBatch() {
        BankAccount bankAccount = new BankAccount();
        bankAccount.setBalance(800.0);
        bankAccount.setNumber("123456789");
        bankAccount.setPin("1234");
        bankAccount.setOverDraft(200.0);
        BankAccount bankAccount2 = new BankAccount();
        bankAccount.setBalance(800.0);
        bankAccount.setNumber("11111111");
        bankAccount.setPin("1234");
        bankAccount.setOverDraft(200.0);

        List<BankAccount> bankAccountList = Arrays.asList(bankAccount, bankAccount2);
        List<BankAccount> bankAccountList1 = bankAccountService.insertBankAccountBatch(bankAccountList);
        assertThat(bankAccountList).hasSize(bankAccountList1.size()).hasSameElementsAs(bankAccountList1);


    }


    @Test
    @DisplayName("getBankAccountWrongPin")
    void getBankAccountWrongPin() {
        assertExceptionAndMessage(WrongPinException.class,
                () -> bankAccountService.getBankAccount(new BankAccountLogin("123456789", "1243")),
                "You entered the wrong pin. Try again");

    }

    @Test
    @DisplayName("getBankAccountPinWithErrors")
    void getBankAccountPinWithErrors() {
        assertExceptionAndMessage(InputErrorException.class,
                () -> bankAccountService.getBankAccount(new BankAccountLogin("123456789", "12")),
                "Pin must be 4 digits");
        assertExceptionAndMessage(InputErrorException.class,
                () -> bankAccountService.getBankAccount(new BankAccountLogin("123456789", "ahvfidhivd")),
                "Pin must be 4 digits");
        assertExceptionAndMessage(InputErrorException.class,
                () -> bankAccountService.getBankAccount(new BankAccountLogin("123456789", "-1")),
                "Pin must be 4 digits");
        assertExceptionAndMessage(InputErrorException.class,
                () -> bankAccountService.getBankAccount(new BankAccountLogin("123456789", "12345")),
                "Pin must be 4 digits");
    }

    @Test
    @DisplayName("getBankAccountAccountNumberWithErrors")
    void getBankAccountAccountNumberWithErrors() {
        assertExceptionAndMessage(InputErrorException.class,
                () -> bankAccountService.getBankAccount(new BankAccountLogin("-1", "1234")),
                "AccountNumber invalid");
        assertExceptionAndMessage(InputErrorException.class,
                () -> bankAccountService.getBankAccount(new BankAccountLogin("feasfsf", "1234")),
                "AccountNumber invalid");
    }


    @Test
    @DisplayName("getBankAccountNotFound")
    void getBankAccountNotFound() {
        final BankAccountLogin bankAccountLogin = new BankAccountLogin("123456788", "1234");
        assertExceptionAndMessage(EntityNotFoundException.class,
                () -> bankAccountService.getBankAccount(bankAccountLogin),
                "Unable to find Bank Account with account number " + bankAccountLogin.getAccountNumber());

    }

    @Test
    @DisplayName("getBankAccountSuccess")
    void getBankAccountSuccess() {
        BankAccount bankAccount = getMockBankAccount();

        BankAccount bankAccount2 = assertDoesNotThrow(() -> bankAccountService.getBankAccount(new BankAccountLogin(bankAccount.getNumber(), bankAccount.getPin())));
        assertEquals(bankAccount2, bankAccount);
    }

    @Test
    @DisplayName("updateBankAccountBalanceAmmountBellowOverDraft")
    void updateBankAccountBalanceAmmountBellowOverDraft() {
        BankAccount bankAccount = getMockBankAccount();

        BankAccountLogin bankAccountLogin = new BankAccountLogin(bankAccount.getNumber(), bankAccount.getPin());
        assertExceptionAndMessage(InputErrorException.class,
                () -> bankAccountService.updateBankAccountBalance(bankAccountLogin, -500.00),
                "New Balance is too low");

    }


    @Test
    @DisplayName("updateBankAccountBalance")
    void updateBankAccountBalance() {
        BankAccount bankAccount = getMockBankAccount();


        BankAccountLogin bankAccountLogin = new BankAccountLogin(bankAccount.getNumber(), bankAccount.getPin());
        Double newBalance = 500d;
        //returned bankAccount
        BankAccount bankAccount1 = assertDoesNotThrow(() -> bankAccountService.updateBankAccountBalance(bankAccountLogin, newBalance));
        //requested bankAccount
        BankAccount bankAccount2 = assertDoesNotThrow(() -> bankAccountService.getBankAccount(bankAccountLogin));


        assertEquals(newBalance, bankAccount1.getBalance(), bankAccount2.getBalance());
    }


}