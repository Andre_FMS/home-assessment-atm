package com.zinkoworks.ATMProject.service;

import com.zinkoworks.ATMProject.dto.BalanceOperationResult;
import com.zinkoworks.ATMProject.dto.BankAccountLogin;
import com.zinkoworks.ATMProject.dto.WithdrawOperationResult;
import com.zinkoworks.ATMProject.exceptions.InputErrorException;
import com.zinkoworks.ATMProject.exceptions.NotEnoughBalanceException;
import com.zinkoworks.ATMProject.exceptions.NotEnoughChangeException;
import com.zinkoworks.ATMProject.model.BankAccount;
import com.zinkoworks.ATMProject.model.MoneyNotesQuantity;
import com.zinkoworks.ATMProject.model.utils.MoneyNotes;
import com.zinkoworks.ATMProject.repository.NotesQuantityRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Map;

import static com.zinkoworks.ATMProject.utils.TestUtils.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.entry;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
class AtmMachineServiceTest {

    @Autowired
    AtmMachineService atmMachineService;

    @MockBean
    NotesQuantityRepository notesQuantityRepository;

    @MockBean
    BankAccountService bankAccountService;


    @BeforeEach
    void setUp() throws Exception {
        Mockito.when(notesQuantityRepository.findAll()).thenReturn(getMoneyNotesQuantityMock());
        Mockito.when(notesQuantityRepository.findAllMap()).thenCallRealMethod();
        Mockito.when(notesQuantityRepository.saveAll(Mockito.any(List.class))).thenAnswer(i -> i.getArguments()[0]);
        Mockito.when(bankAccountService.getBankAccount(generateBankAccountLogin(getMockBankAccount()))).thenReturn(getMockBankAccount());
    }


    @Test
    void saveMoneyNotesQuantity() {
        List<MoneyNotesQuantity> moneyNotesQuantityList = getMoneyNotesQuantityMock();
        List<MoneyNotesQuantity> moneyNotesQuantityList1 = atmMachineService.saveMoneyNotesQuantity(moneyNotesQuantityList);
        assertThat(moneyNotesQuantityList).hasSize(moneyNotesQuantityList1.size()).hasSameElementsAs(moneyNotesQuantityList1);
    }

    @Test
    void WithdrawMoneyAmmountNotPositive() {

        assertExceptionAndMessage(InputErrorException.class,
                () -> atmMachineService.withdrawMoney(generateBankAccountLogin(getMockBankAccount()), -200),
                "Ammont to withdraw must be positive");
    }


    @Test
    void withdrawMoneyNotEnoughBalance() {
        BankAccount bankAccount = getMockBankAccount();
        BankAccountLogin bankAccountLogin = generateBankAccountLogin(bankAccount);
        double ammountToWithdraw = bankAccount.getBalance() + bankAccount.getOverDraft() + 100;

        assertExceptionAndMessage(NotEnoughBalanceException.class,
                () -> atmMachineService.withdrawMoney(bankAccountLogin, (int) ammountToWithdraw),
                "Not enough balance in your bank account. Please choose a lower value to withdraw.");


    }

    @Test
    @DisplayName("getBalanceSuccess")
    void getBalanceSuccess() {
        BankAccount bankAccount = getMockBankAccount();
        BalanceOperationResult balanceOperationResult = assertDoesNotThrow(() -> atmMachineService.getBalance(new BankAccountLogin(bankAccount.getNumber(), bankAccount.getPin())));
        assertEquals(balanceOperationResult.getCurrentBalance(), bankAccount.getBalance());
        Integer maxWithdraw = balanceOperationResult.getCurrentBalance() < 0 ? 0 : (int) Math.min(bankAccount.getBalance(), atmMachineService.getSumOfNotes());
        assertEquals(balanceOperationResult.getMaxWithdrawAvailable(), maxWithdraw);
    }

    @Test
    void withdrawMoneyNegativeBalance() throws Exception {
        BankAccount bankAccount = new BankAccount();
        bankAccount.setNumber("123456789");
        bankAccount.setBalance(-100.0);
        bankAccount.setOverDraft(200.0);
        bankAccount.setPin("1234");
        BankAccountLogin bankAccountLogin = generateBankAccountLogin(bankAccount);
        Mockito.when(bankAccountService.getBankAccount(bankAccountLogin)).thenReturn(bankAccount);

        double ammountToWithdraw = bankAccount.getBalance() + bankAccount.getOverDraft() + 100;

        assertExceptionAndMessage(NotEnoughBalanceException.class,
                () -> atmMachineService.withdrawMoney(bankAccountLogin, (int) ammountToWithdraw),
                "You cannot withdraw money because your account has a negative balance.");


    }


    @Test
    void withdrawMoneyNotEnoughChange() {
        BankAccount bankAccount = getMockBankAccount();
        BankAccountLogin bankAccountLogin = generateBankAccountLogin(bankAccount);
        Integer moneyMocked = atmMachineService.getSumOfNotes();
        double ammountToWithdraw = moneyMocked + 100;

        assertExceptionAndMessage(NotEnoughChangeException.class,
                () -> atmMachineService.withdrawMoney(bankAccountLogin, (int) ammountToWithdraw),
                "Not enough change in the ATM. Please choose a lower value to withdraw.");
    }

    @Test
    void withdrawMoneySuccess() {
        BankAccount bankAccount = getMockBankAccount();
        BankAccountLogin bankAccountLogin = generateBankAccountLogin(bankAccount);
        Integer moneyMocked = atmMachineService.getSumOfNotes();
        double ammountToWithdraw = Math.min(moneyMocked, bankAccount.getBalance()) / 5;
        WithdrawOperationResult withdrawOperationResult = assertDoesNotThrow(() -> atmMachineService.withdrawMoney(bankAccountLogin, (int) ammountToWithdraw));
        Map<MoneyNotes, Integer> resultChangeMap = withdrawOperationResult.getMoneyNotesQuantityMap();
        assertEquals(withdrawOperationResult.getCurrentBalance(), bankAccount.getBalance() - ammountToWithdraw);
        assertThat(resultChangeMap).contains(entry(MoneyNotes.TEN, 0))
                .contains(entry(MoneyNotes.TWENTY, 0))
                .contains(entry(MoneyNotes.FIFTY, 6))
                .contains(entry(MoneyNotes.HUNDRED, 0))
                .contains(entry(MoneyNotes.TWO_HUNDRED, 0))
                .contains(entry(MoneyNotes.FIVE_HUNDRED, 0));
    }


}